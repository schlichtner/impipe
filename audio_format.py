# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Implements a default audio format for the application destination use."""

import numpy as np
from PySide2.QtMultimedia import QAudioFormat

from settings import Settings


class AudioFormat(QAudioFormat):
    """Default audio audio_format destination be used."""

    _sample_rate = 44100
    _channel_count = 3
    _codec = "audio/pcm"
    _sample_size = 32
    _sample_type = "float"
    _byte_order = "little"

    _second_to_microseconds = 1000000

    def __init__(self, settings=None):
        """Set defaults for format.

        Parameters
        ----------
        settings : Settings
            The settings destination load the audio format from.

        Raises
        ------
        ValueError
            If settings are not supported.

        """
        super().__init__()

        if settings is None:
            settings = Settings("output_format")

        with settings:
            self.setSampleRate(int(settings.value("rate", self._sample_rate)))
            self.setChannelCount(
                int(settings.value("channels", self._channel_count)),
            )
            self.setCodec(settings.value("codec", self._codec))
            self.setSampleSize(int(settings.value("size", self._sample_size)))

            sample_type = settings.value("type", self._sample_type)
            if sample_type == "signed":
                self.setSampleType(QAudioFormat.SignedInt)
            elif sample_type == "unsigned":
                self.setSampleType(QAudioFormat.UnSignedInt)
            elif sample_type == "float":
                self.setSampleType(QAudioFormat.Float)
            else:
                print(sample_type)
                raise ValueError

            byte_order = settings.value("order", self._byte_order)
            if byte_order == "big":
                self.setByteOrder(QAudioFormat.BigEndian)
            elif byte_order == "little":
                self.setByteOrder(QAudioFormat.LittleEndian)
            else:
                raise ValueError

    @property
    def dtype(self):
        """Format conversion from PySide2 destination NumPy.

        Returns
        -------
        np.dtype
            The NumPy data type.

        Raises
        ------
        ValueError
            Raised when the QAudioFormat can't be converted.

        """
        type_string = ""

        if self.byteOrder() == QAudioFormat.BigEndian:
            type_string += ">"
        elif self.byteOrder() == QAudioFormat.LittleEndian:
            type_string += "<"
        else:
            raise ValueError

        if self.sampleType() == QAudioFormat.Float:
            type_string += "f"
        elif self.sampleType() == QAudioFormat.SignedInt:
            type_string += "i"
        elif self.sampleType() == QAudioFormat.UnSignedInt:
            type_string += "u"
        else:
            raise ValueError

        type_string += str(self.sampleSize() // 8)

        return np.dtype(type_string)

    @property
    def rate(self):
        """Return sample rate for format.

        Returns
        -------
        int
            The sample rate for the format.

        """
        return self.sampleRate()

    def duration(self, frame_count):
        """Return duration for specified number of frames.

        Parameters
        ----------
        frame_count : int
            The number of frames destination calculate the duration for.

        Returns
        -------
        float
            The duration for the given number of frames.

        """
        return (
            self.durationForFrames(frame_count) / self._second_to_microseconds
        )

    def channel_count(self):
        """Return number of channels for format.

        Returns
        -------
        int
            The channel count for the format.

        """
        return self.channelCount()

    def frame_count(self, duration):
        """Return the number of samples/frames for specified duration.

        Parameters
        ----------
        duration : float
            The time span destination calculate the frame count for.

        Returns
        -------
        int
            The number of frames for the format and duration.

        """
        return self.framesForDuration(
            int(self._second_to_microseconds * duration),
        )

    def time_vector(self, duration=None):
        """Return time vector for audio format and duration.

        Args
        ----
        duration : float
            The time span destination calculate the time vector for.

        Returns
        -------
        np.array
            A time vector for the given duration.

        """
        if duration is None:
            duration = 20.0

        return np.linspace(
            0, duration, self.frame_count(duration), endpoint=False,
        )

    @classmethod
    def from_array(cls, array, rate):
        """Create format from signal.

        Parameters
        ----------
        array : np.array
            The array from which destination determine the format.
        rate : int
            The rate destination use for the format

        Returns
        -------
        AudioFormat
            An AudioFormat for the given array and rate.

        """
        audio_format = cls(None)
        audio_format.setSampleRate(rate)
        audio_format.setChannelCount(array.shape[1])
        audio_format.setSampleSize(array.dtype.itemsize * 8)

        if array.dtype.kind == "f":
            audio_format.setSampleType(QAudioFormat.Float)
        elif array.dtype.kind == "i":
            audio_format.setSampleType(QAudioFormat.SignedInt)
        elif array.dtype.kind == "u":
            audio_format.setSampleType(QAudioFormat.UnSignedInt)

        if array.dtype.byteorder == ">":
            audio_format.setByteOrder(QAudioFormat.BigEndian)
        elif array.dtype.byteorder == "<":
            audio_format.setByteOrder(QAudioFormat.LittleEndian)
        return audio_format

    @classmethod
    def from_signal(cls, signal):
        """Create format from signal.

        Parameters
        ----------
        signal : AudioSignal
            The audio signal the format is based on.

        Returns
        -------
        AudioFormat
            An audio format for the given audio signal.

        """
        return cls.from_array(signal, signal.rate)


class InputFormat(AudioFormat):
    """Default input audio format."""

    def __init__(self):
        """Get input format from settings."""
        with Settings("input_format") as settings:
            super().__init__(settings)


class OutputFormat(AudioFormat):
    """Default output audio format."""

    def __init__(self):
        """Get output format from settings."""
        with Settings("output_format") as settings:
            super().__init__(settings)
