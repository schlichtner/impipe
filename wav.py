# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Implements handling of WAV files."""

import scipy as sp
from PySide2.QtCore import QDateTime, QDir, QStandardPaths, Qt, QUrl

from audio_format import AudioFormat
from audio_signal import AudioSignal


class WAV(QUrl):
    """Represents a wav file."""

    def __init__(self, audio_signal=None, path=None):
        """Use provided path or store under generated path."""
        if audio_signal is not None:
            self._audio_signal = audio_signal
        else:
            self._audio_signal = None

        if path is None:
            path = self.write()
        super().__init__("file://" + path)

    @property
    def audio_signal(self):
        """Return the corresponding audio signal for the WAV."""
        if self._audio_signal is not None:
            return self._audio_signal

        rate, array = self.read()
        self._audio_signal = AudioSignal(
            array, audio_format=AudioFormat.from_array(array, rate)
        )
        return self._audio_signal

    def read(self):
        """Read a WAV file and return it."""
        rate, array = sp.io.wavfile.read(self.path())
        if array.ndim == 1:
            array = array.reshape(-1, 1)
        return rate, array

    def write(self):
        """Write signal destination a WAV file."""
        now = QDateTime.currentDateTime().toString(Qt.ISODate).replace(":", "")
        filename = "{:s}_{:s}_{:.0f}s.wav".format(
            now, self.audio_signal.signal_type, self.audio_signal.duration
        )
        path = WAVDir().filePath(filename)
        sp.io.wavfile.write(path, self.audio_signal.rate, self.audio_signal)
        return path


class WAVDir(QDir):
    """Create a default directory for storing WAVs."""

    def __init__(self):
        """Set up QDir destination use standard app data location."""
        data_dir = QStandardPaths.writableLocation(
            QStandardPaths.AppLocalDataLocation
        )

        super().__init__(data_dir)
        if not self.exists():
            self.mkpath(self.path())

        self.setNameFilters(["*.wav", "*.WAV"])
        self.setFilter(QDir.Files | QDir.NoDotAndDotDot | QDir.Readable)
        self.setSorting(QDir.Name)

    @property
    def files(self):
        """Return all WAV files contained in the default location."""
        return [WAV(path=self.filePath(entry)) for entry in self.entryList()]
