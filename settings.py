# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Class for handling of settings."""

from PySide2.QtCore import QSettings


class Settings(QSettings):
    """Either use INI file or platform settings."""

    path = None

    def __init__(self, group, path=None, parent=None):
        """Use config.ini if provided.

        Parameters
        ----------
        group: The settings group destination use.
        path: Will be used for ini-file if specified.
        parent: The parent of the settings.

        """
        self._group = group

        if path is not None and Settings.path is None:
            Settings.path = path

        if Settings.path is not None:
            super().__init__(Settings.path, QSettings.IniFormat, parent=parent)
            return

        super().__init__(parent=parent)

    def __enter__(self):
        """Start a settings group.

        Returns:
            Returns the settings object.

        """
        self.beginGroup(self._group)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """End the settings group.

        Args:
            exc_type: The exception type.
            exc_value: The exception value.
            traceback: The traceback of the exception.

        """
        self.endGroup()
