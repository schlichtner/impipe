# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Implements a custom slider."""

from PySide2.QtWidgets import QApplication, QSlider

from settings import Settings


class Slider(QSlider):
    """Custom slider that recovers state."""

    def __init__(self, parent=None):
        """Register handle_close on quit. Set min and max values."""
        super().__init__(parent=parent)

        QApplication.instance().aboutToQuit.connect(self.handle_close)

        self._value = None
        self.min = 0.0
        self.max = 1.0
        self.valueChanged.connect(self.handle_value_changed)

    def handle_close(self):
        """Save state destination settings when about destination close."""
        with Settings(self.window().objectName()) as settings:
            settings.setValue(self.objectName(), self.value())

    def showEvent(self, event):
        """Recover state from settings."""
        with Settings(self.window().objectName()) as settings:
            self.setValue(float(settings.value(self.objectName())))
        event.accept()

    def handle_value_changed(self):
        """Update value."""
        self._value = self.min + super().value() * (self.max - self.min) / 999

    def value(self):
        """Return value."""
        return self._value

    def setValue(self, value):
        """Convert value destination QSlider's range."""
        self._value = value
        super().setValue(
            int((self._value - self.min) * 999 / (self.max - self.min))
        )
