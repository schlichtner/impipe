# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Implements custom buttons."""

from enum import Enum

from PySide2.QtCore import QObject, Signal
from PySide2.QtGui import QShowEvent
from PySide2.QtWidgets import QPushButton, QWidget


class BinaryState(Enum):
    """State enum for the BinaryButton class."""

    STOPPED = 0
    RUNNING = 1


class BinaryButton(QPushButton):
    """Button with two states."""

    start = Signal()
    stop = Signal()

    def __init__(self, parent: QWidget = None):
        """Set initial state.

        Parameters
        ----------
        parent : QWidget
            The parent widget.

        """
        super().__init__(parent=parent)

        self.state = BinaryState.STOPPED
        self.initial_text = ""
        self.clicked.connect(self.handle_clicked)

    def showEvent(self, event: QShowEvent):
        """Store default label.

        Parameters
        ----------
        event : QShowEvent
            The show event.

        """
        del event  # unused
        self.initial_text = self.text()

    def handle_clicked(self):
        """Change state when clicked."""
        if self.state == BinaryState.STOPPED:
            self.state = BinaryState.RUNNING
            self.setText(QObject.property(self, "text2"))
            self.start.emit()
        else:
            self.state = BinaryState.STOPPED
            self.setText(self.initial_text)
            self.stop.emit()

    def show_stop(self):
        """Show initial label again."""
        self.state = BinaryState.STOPPED
        self.setText(self.initial_text)


class TripleState(Enum):
    """State enum for the TripleButton class."""

    STOPPED = 0
    RUNNING_1 = 1
    RUNNING_2 = 2


class TripleButton(QPushButton):
    """Button with three states."""

    start = Signal()
    start2 = Signal()
    stop = Signal()

    def __init__(self, parent=None):
        """Set initial state.

        Parameters
        ----------
        parent : PySide2.QtWidgets.QWidget
            The parent widget.

        """
        super().__init__(parent=parent)

        self.state = TripleState.STOPPED
        self.initial_text = ""
        self.clicked.connect(self.handle_clicked)

    def showEvent(self, event):
        """Store default label.

        Parameters
        ----------
        event : PySide2.QtGui.QShowEvent
            The show event.

        """
        del event  # unused
        self.initial_text = self.text()

    def handle_clicked(self):
        """Change state when clicked."""
        if self.state == TripleState.STOPPED:
            self.state = TripleState.RUNNING_1
            self.setText(QObject.property(self, "text2"))
            self.start.emit()
        elif self.state == TripleState.RUNNING_1:
            self.state = TripleState.RUNNING_2
            self.setText(QObject.property(self, "text3"))
            self.start2.emit()
        elif self.state == TripleState.RUNNING_2:
            self.state = TripleState.STOPPED
            self.setText(self.initial_text)
            self.stop.emit()

    def show_stop(self):
        """Show initial state again."""
        self.state = TripleState.STOPPED
        self.setText(self.initial_text)

    def show_start2(self):
        """Show intermediate text."""
        self.state = TripleState.RUNNING_2
        self.setText(QObject.property(self, "text3"))
