![alt text](main_window.png "Main Window")

# How to test the current status under Windows

## Download and install python
[Python](https://www.python.org/downloads/)

## Download and install Git
[Git](https://git-scm.com/downloads)

## Get this repository

```cd Downloads```

```git clone https://gitlab.com/schlichtner/impipe.git```

Now you have a folder called _impipe_ in your Downloads folder

## Create a python venv for this program to run in

```cd impipe```

```py -m venv .venv```

```.venv\Scripts\activate.bat```

## Update pip

```py -m pip install --upgrade pip```

## Install dependencies

```py -m pip install -r requirements.txt```

## Run the program

```main.py```

## Get the latest version via Git

```git pull origin master```
