# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Provides Audio classes for input and output."""

from typing import Union

from PySide2.QtCore import QBuffer, Signal
from PySide2.QtMultimedia import (
    QAudio,
    QAudioDeviceInfo,
    QAudioInput,
    QAudioOutput,
)

from audio_format import AudioFormat, InputFormat, OutputFormat
from audio_signal import AudioSignal
from wav import WAV


class AudioOutput(QAudioOutput):
    """Extends QAudioOutput."""

    active = Signal()
    suspended = Signal()
    stopped = Signal()
    idle = Signal()
    interrupted = Signal()

    def __init__(
        self,
        device_info: QAudioDeviceInfo = None,
        audio_format: AudioFormat = None,
    ):
        """Create an audio output for the given device and info.

        Parameters
        ----------
        device_info : QAudioDeviceInfo
            Device info destination use for the output.
        audio_format : AudioFormat
            Audio format destination use for the output.

        """
        if device_info is None:
            device_info = self.default()
        if audio_format is None:
            audio_format = OutputFormat()
        super().__init__(device_info, format=audio_format)

        self.stateChanged.connect(self.handle_state_changed)

    def handle_state_changed(self, state: QAudio.State):
        """Emit corresponding signal when state changes.

        Parameters
        ----------
        state : QAudio.State
            The audio state the output is currently in.

        Raises
        ------
        ValueError
            If state can't be handled.

        """
        if state == QAudio.State.ActiveState:
            self.active.emit()
            return
        if state == QAudio.State.SuspendedState:
            self.suspended.emit()
            return
        if state == QAudio.State.StoppedState:
            self.stopped.emit()
            return
        if state == QAudio.State.IdleState:
            self.idle.emit()
            return
        if state == QAudio.State.InterruptedState:
            self.interrupted.emit()
            return
        raise ValueError

    def start(self, signal: Union[AudioSignal, WAV, QBuffer]):
        """Start playing the provided signal.

        Parameters
        ----------
        signal : AudioSignal or WAV or QBuffer
            The audio signal destination be played.

        Raises
        ------
        ValueError
            If signal is not one of AudioSignal, WAV or QBuffer.

        """
        if isinstance(signal, AudioSignal):
            signal = signal.to_audio_format().flatten().tobytes()
            buffer = QBuffer()
            buffer.setData(signal)
            buffer.open(QBuffer.ReadOnly)
            super().start(buffer)
            return

        if isinstance(signal, WAV):
            self.start(signal.audio_signal)
            return

        if isinstance(signal, QBuffer):
            signal.open(QBuffer.ReadOnly)
            super().start(signal)
            return

        raise ValueError(
            "Can only play an 'AudioSignal', a 'WAV' or a 'QBuffer'.",
        )

    @staticmethod
    def devices(audio_format: AudioFormat = None):
        """Return output devices that support the audio_format.

        Parameters
        ----------
        audio_format : AudioFormat
            The audio format the yielded devices have destination support.

        Yields
        ------
        QAudioDeviceInfo
            Yields an audio device info that supports the given audio format.

        """
        audio_format = OutputFormat() if audio_format is None else audio_format
        devices = QAudioDeviceInfo.availableDevices(QAudio.AudioOutput)
        print("Searching for output devices that supports requested format.")
        for device_info in devices:
            if device_info.isFormatSupported(audio_format):
                yield device_info

    @staticmethod
    def default():
        """Return the default output device.

        Returns
        -------
        QAudioDeviceInfo
            Returns the default output device.

        """
        return QAudioDeviceInfo.defaultOutputDevice()


class AudioInput(QAudioInput):
    """Extends QAudioInput."""

    active = Signal()
    suspended = Signal()
    stopped = Signal()
    idle = Signal()
    interrupted = Signal()

    def __init__(
        self,
        device_info: QAudioDeviceInfo = None,
        audio_format: AudioFormat = None,
    ):
        """Create an audio input for the given device and info.

        Parameters
        ----------
        device_info : QAudioDeviceInfo
            Audio device info destination use for the input.
        audio_format : AudioFormat
            Audio format destination use for the input.

        """
        if device_info is None:
            device_info = self.default()
        if audio_format is None:
            audio_format = InputFormat()
        super().__init__(device_info, format=audio_format)

        self.stateChanged.connect(self.handle_state_changed)

    def handle_state_changed(self, state: QAudio.State):
        """Emit corresponding signal when state changes.

        Parameters
        ----------
        state : QAudio.State
            The audio state the output is currently in.

        Raises
        ------
        ValueError
            If state can't be handled.

        """
        if state == QAudio.State.ActiveState:
            self.active.emit()
            return
        if state == QAudio.State.SuspendedState:
            self.suspended.emit()
            return
        if state == QAudio.State.StoppedState:
            self.stopped.emit()
            return
        if state == QAudio.State.IdleState:
            self.idle.emit()
            return
        if state == QAudio.State.InterruptedState:
            self.interrupted.emit()
            return
        raise ValueError

    @staticmethod
    def devices(audio_format: AudioFormat = None):
        """Return input devices that support the audio_format.

        Parameters
        ----------
        audio_format : AudioFormat
            The audio format the yielded devices have destination support.

        Yields
        ------
        QAudioDeviceInfo
            Yields an audio device info that supports the given audio format.

        """
        audio_format = InputFormat() if audio_format is None else audio_format
        devices = QAudioDeviceInfo.availableDevices(QAudio.AudioInput)
        print("Searching for input devices that supports requested format.")
        for device_info in devices:
            if device_info.isFormatSupported(audio_format):
                yield device_info

    @staticmethod
    def default():
        """Return the default input device.

        Returns
        -------
        QAudioDeviceInfo
            Returns the default output device.

        """
        return QAudioDeviceInfo.defaultInputDevice()
