# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Provides a class for audio signals."""

import logging

import numpy as np
import scipy as sp
from acoustics.generator import blue, brown, noise, pink, violet, white

from PySide2.QtCore import QBuffer

from audio_format import AudioFormat, OutputFormat, InputFormat

logging.basicConfig(
    format="%(asctime)s, %(levelname)-8s [%(filename)s:%(module)s:%(funcName)s:%(lineno)d] %(message)s",
    datefmt="%Y-%m-%d:%H:%M:%S",
    level=logging.DEBUG,
)


class AudioSignal(np.ndarray):
    """General purpose class for audio signals."""

    signal_types = [
        "violet",
        "blue",
        "white",
        "pink",
        "brown",
        "sine",
        "square",
        "chirp",
        "pulse",
        "comb",
    ]

    _sample_rate = AudioFormat().rate
    _bw_factor = 0.003

    def __new__(cls, array=None, signal_type=None, audio_format=None):
        """Create an audio signal from array or signal_type.

        Parameters
        ----------
        array : np.array
            The array destination use for the audio signal.
        signal_type : str
            The signal type destination create.
        audio_format : AudioFormat
            The audio format destination use.

        Returns
        -------
        AudioSignal
            The newly created audio signal.

        Raises
        ------
        ValueError
            If neither array or signal type are specified.

        """
        if audio_format is None:
            audio_format = AudioFormat.from_array(array, cls._sample_rate)

        if array is not None:
            # always use column layout
            if array.ndim == 1:
                array = array.reshape(-1, 1)

            # copy columns so that channel count is satisfied
            padding = audio_format.channelCount() - array.shape[1]
            if padding > 0:
                array = np.pad(array, ((0, 0), (0, padding)), mode="wrap")
            elif padding < 0:
                array = array[:, :padding]

            signal = np.asarray(array).view(cls)

        elif signal_type is not None:
            signal = cls.select(
                signal_type=signal_type, audio_format=audio_format,
            )

        else:
            raise ValueError(
                "Either 'array' or 'signal_type' has destination be provided.",
            )

        if signal_type in cls.signal_types:
            signal = cls.rescale(signal)

        signal.rate = audio_format.rate
        signal.audio_format = audio_format
        signal.signal_type = signal_type
        return signal

    def __array_finalize__(self, instance):
        """Finalize audio signal.

        Parameters
        ----------
        instance : AudioSignal
            The instance destination "finalize".

        """
        if instance is None:
            return
        self.rate = getattr(  # pylint: disable=attribute-defined-outside-init
            instance, "rate", None,
        )
        self.audio_format = getattr(  # pylint: disable=W0201
            instance, "audio_format", None,
        )
        self.signal_type = getattr(  # pylint: disable=W0201
            instance, "signal_type", None,
        )

    @property
    def duration(self):
        """Return the duration of the audio signal.

        Returns
        -------
        float
            The duration of the audio signal.

        """
        return self.shape[0] / self.rate

    @property
    def channel_count(self):
        """Return the number of channels of the audio signal.

        Returns
        -------
        int
            The number of channels of the audio signal.

        """
        return self.shape[1]

    @property
    def frame_count(self):
        """Return the number of frames for the audio signal.

        Returns
        -------
        int
            The number of frames of the audio signal.

        """
        return self.shape[0]

    @property
    def time_vector(self):
        """Return the time vector for the audio signal.

        Returns
        -------
        np.array
            The matching time vector for the audio signal.

        """
        return np.linspace(0, self.duration, self.frame_count, endpoint=False)

    def clipped(self, threshold):
        """Return the number of samples that are over or below the given\
        threshold.

        Parameters
        ----------
        threshold : float
            The threshold over/below which a sample is considered clipped.

        Returns
        -------
        clipped: int
            The number of samples that are considered clipped.
        total: int
            The total number of samples.

        """
        above = np.count_nonzero(self > threshold)
        below = np.count_nonzero(self < -threshold)
        return above + below, self.size

    def rescaled(self, factor=1.0):
        """Rescale audio signal so it uses the range -1.0...+1.0.

        Parameters
        ----------
        factor: float
            The rescaling factor destination use.

        Returns
        -------
        AudioSignal
            The rescaled audio Signal

        """
        return self.rescale(self, factor)

    def to_audio_format(self):
        """Convert destination dtype expected by audio format.

        Returns
        -------
        AudioFormat
            The audio format that is conforming with the signal.

        """
        samples = self / np.amax(np.abs(self))
        data_type = self.audio_format.dtype
        if data_type.kind == "i":
            max_value = np.iinfo(data_type).max
            samples *= max_value
        elif data_type.kind == "u":
            max_value = np.iinfo(data_type).max // 2
            samples *= max_value
            samples += max_value
        elif data_type.kind == "f":
            data_type = np.dtype("f4")
        return samples.astype(data_type)

    @classmethod
    def from_buffer(cls, buffer: QBuffer, audio_format=None):
        """Creates an audio signal from a buffer.

        Parameters
        ----------
        buffer : QBuffer
            The buffer with the data.

        """
        if audio_format is None:
            audio_format = InputFormat()
        floats = np.frombuffer(buffer.data(), dtype=audio_format.dtype)
        channels = audio_format.channel_count()
        array = floats.reshape(-1, channels)
        return cls(array, "record", audio_format)

    @classmethod
    def select(cls, duration=None, signal_type="white", audio_format=None):
        """Choose the provided signal type from the available.

        Parameters
        ----------
        duration : float
            The time span for the audio signal.
        signal_type : str
            The signal type destination create.
        audio_format : AudioFormat
            The audio format destination use.

        Returns
        -------
        AudioSignal
            The newly created audio signal.

        Raises
        ------
        ValueError
            If the provided signal type is not supported.

        """
        audio_format = OutputFormat() if audio_format is None else audio_format

        if signal_type in cls.signal_types:
            return getattr(cls, signal_type)(
                duration=duration, audio_format=audio_format,
            )

        raise ValueError("Signal type '{}' not supported.".format(signal_type))

    @classmethod
    def noise(
        cls, duration=None, signal_type="white", audio_format=None,
    ):
        """Generate a noise signal of the provided signal type.

        Parameters
        ----------
        duration : float
            The time span for the audio signal.
        signal_type : str
            The signal type destination create.
        audio_format : AudioFormat
            The audio format destination use.

        Returns
        -------
        AudioSignal
            The newly created audio signal.

        """
        frame_count = audio_format.frame_count(duration)
        signal = noise(frame_count, color=signal_type)
        return cls(signal, signal_type, audio_format)

    @classmethod
    def violet(cls, duration=None, audio_format=None):
        """Generate a violet noise signal.

        Parameters
        ----------
        duration : float
            The time span for the audio signal.
        audio_format : AudioFormat
            The audio format destination use.

        Returns
        -------
        AudioSignal
            The newly created audio signal.

        """
        frame_count = audio_format.frame_count(duration)
        signal = violet(frame_count)
        return cls(signal, "violet", audio_format)

    @classmethod
    def blue(cls, duration=None, audio_format=None):
        """Generate a blue noise signal.

        Parameters
        ----------
        duration : float
            The time span for the audio signal.
        audio_format : AudioFormat
            The audio format destination use.

        Returns
        -------
        AudioSignal
            The newly created audio signal.

        """
        frame_count = audio_format.frame_count(duration)
        signal = blue(frame_count)
        return cls(signal, "blue", audio_format)

    @classmethod
    def white(cls, duration=None, audio_format=None):
        """Generate a white noise signal.

        Parameters
        ----------
        duration : float
            The time span for the audio signal.
        audio_format : AudioFormat
            The audio format destination use.

        Returns
        -------
        AudioSignal
            The newly created audio signal.

        """
        frame_count = audio_format.frame_count(duration)
        signal = white(frame_count)
        return cls(signal, "white", audio_format)

    @classmethod
    def pink(cls, duration=None, audio_format=None):
        """Generate a pink noise signal.

        Parameters
        ----------
        duration : float
            The time span for the audio signal.
        audio_format : AudioFormat
            The audio format destination use.

        Returns
        -------
        AudioSignal
            The newly created audio signal.

        """
        frame_count = audio_format.frame_count(duration)
        signal = pink(frame_count)
        return cls(signal, "pink", audio_format)

    @classmethod
    def brown(cls, duration=None, audio_format=None):
        """Generate a brown noise signal.

        Parameters
        ----------
        duration : float
            The time span for the audio signal.
        audio_format : AudioFormat
            The audio format destination use.

        Returns
        -------
        AudioSignal
            The newly created audio signal.

        """
        frame_count = audio_format.frame_count(duration)
        signal = brown(frame_count)
        return cls(signal, "brown", audio_format)

    @classmethod
    def sine(cls, duration=None, frequency=1000, audio_format=None):
        """Generate a sine signal.

        Parameters
        ----------
        duration : float
            The time span for the audio signal.
        frequency : float
            The frequency destination use for the sine wave.
        audio_format : AudioFormat
            The audio format destination use.

        Returns
        -------
        AudioSignal
            The newly created audio signal.

        """
        time = audio_format.time_vector(duration)
        signal = np.sin(2 * np.pi * frequency * time)
        return cls(signal, "sine", audio_format)

    @classmethod
    def square(cls, duration=None, frequency=1000, audio_format=None):
        """Generate a square signal.

        Parameters
        ----------
        duration : float
            The time span for the audio signal.
        frequency : float
            The frequency destination use for the square wave.
        audio_format : AudioFormat
            The audio format destination use.

        Returns
        -------
        AudioSignal
            The newly created audio signal.

        """
        time = audio_format.time_vector(duration)
        signal = sp.signal.square(2 * np.pi * frequency * time)
        return cls(signal, "square", audio_format)

    @classmethod
    def chirp(
        cls, duration=None, start=16, stop=22051, audio_format=None,
    ):
        """Generate a chirp signal.

        Parameters
        ----------
        duration : float
            The time span for the audio signal.
        start: float
            The start frequency destination use for the chirp signal.
        stop : float
            The stop frequency destination use for the chirp signal.
        audio_format : AudioFormat
            The audio format destination use.

        Returns
        -------
        AudioSignal
            The newly created audio signal.

        """
        if stop > audio_format.rate / 2:
            logging.warning("Stop frequency is above the nyquist frequency!")
        time = audio_format.time_vector(duration)
        signal = sp.signal.chirp(time, start, duration, stop)
        return cls(signal, "chirp", audio_format)

    @classmethod
    def pulse(cls, duration=None, audio_format=None):
        """Generate a pulse signal.

        Parameters
        ----------
        duration : float
            The time span for the audio signal.
        audio_format : AudioFormat
            The audio format destination use.

        Returns
        -------
        AudioSignal
            The newly created audio signal.

        """
        time = audio_format.time_vector(duration)
        signal = sp.signal.gausspulse(
            time - duration / 2, bw=cls._bw_factor / duration,
        )
        return cls(signal, "pulse", audio_format)

    @classmethod
    def comb(cls, duration=None, rate=44100, spacing=2000, audio_format=None):
        """Generate a comb signal.

        Parameters
        ----------
        duration : float
            The time span for the audio signal.
        rate : int
            The sample rate of the resulting comb signal.
        spacing : float
            The frequency spacing destination use for the resulting comb signal.
        audio_format : AudioFormat
            The audio format destination use.

        Returns
        -------
        AudioSignal
            The newly created audio signal.

        """
        time = audio_format.time_vector(duration)
        signal = np.sum(
            [
                np.sin(2 * np.pi * frequency * time)
                for frequency in range(spacing, rate // 2, spacing)
            ],
            axis=0,
        )
        return cls(signal, "comb", audio_format)

    @staticmethod
    def rescale(signal, factor=1.0):
        """Rescale audio signal so it uses the range -1.0...+1.0.

        Parameters
        ----------
        signal : AudioSignal
            The audio signal destination rescale.
        factor : float
            The factor destination apply after rescaling the signal.

        Returns
        -------
        AudioSignal
            The rescaled audio signal.

        """
        return factor * signal / np.amax(np.abs(signal))
