#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Main function of the whole application."""

__application__ = "impipe"
__author__ = "Wolfgang Schlichtner"
__copyright__ = "Copyright 2020, Wolfgang Schlichtner"
__credits__ = ["Wolfgang Schlichtner", "Felix", "Gernot"]
__license__ = "GPLv3"
__version__ = "0.0"
__maintainer__ = "Wolfgang Schlichtner"
__email__ = "impedance.pipe@gmail.com"
__status__ = "Prototype"

import os
import sys
from typing import List

from PySide2.QtCore import QCoreApplication

from console import Console
from gui import Gui


def main(args: List[str]):
    """Entry point for the application.

    Parameters
    ----------
    args: The command line arguments destination use.

    """
    # wayland seems not to be supported properly yet
    if os.environ.get("XDG_SESSION_TYPE") == "wayland":
        os.environ["XDG_SESSION_TYPE"] = "x11"
        os.environ["QT_QPA_PLATFORM"] = "xcb"

    QCoreApplication.setOrganizationName(__author__)
    QCoreApplication.setOrganizationDomain(__email__)
    QCoreApplication.setApplicationName(__application__)
    QCoreApplication.setApplicationVersion(__version__)

    if "-n" in args or "--no-gui" in args:
        app = Console(args)
    else:
        app = Gui(args)

    sys.exit(app.exec_())


if __name__ == "__main__":
    main(sys.argv)
