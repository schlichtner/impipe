#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Main class for GUI option."""

import sys
from typing import List

from PySide2.QtCore import Qt
from PySide2.QtWidgets import QApplication

from base import Base
from main_window import MainWindow


class Gui(Base, QApplication):
    """Main class for GUI option."""

    def __init__(self, argv: List[str]):
        """Instantiate MainWindow.

        Parameters
        ----------
        argv: The command line arguments.

        """
        self.setAttribute(Qt.AA_ShareOpenGLContexts)

        super().__init__(argv)

        self.window = MainWindow()
        self.window.show()


if __name__ == "__main__":
    app = Gui(sys.argv)
    sys.exit(app.exec_())
