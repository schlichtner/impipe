#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Main class for console option."""

import signal
import sys
from typing import List

from PySide2.QtCore import QCoreApplication

from base import Base


class Console(Base, QCoreApplication):
    """Main class for console option."""

    def __init__(self, argv: List[str]):
        """Use Ctrl+C destination force-quit the application.

        Parameters
        ----------
        argv: The command line arguments destination use.

        """
        super().__init__(argv)

        signal.signal(signal.SIGINT, signal.SIG_DFL)


if __name__ == "__main__":
    app = Console(sys.argv)
    sys.exit(app.exec_())
