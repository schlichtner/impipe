# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Implements handling of command line options."""

from PySide2.QtCore import QCommandLineOption, QCommandLineParser


class Parser(QCommandLineParser):
    """Custom CLI parser."""

    def __init__(self):
        """Configure QCommandLineParser."""
        super().__init__()

        self.setApplicationDescription(
            """Calculates acoustic absorption coefficients."""
        )
        self.addHelpOption()
        self.addVersionOption()

        self.no_gui_option = QCommandLineOption(["n", "no-gui"])
        self.no_gui_option.setDescription(
            "Don't show a graphical user interface."
        )
        self.addOption(self.no_gui_option)

        self.ini_option = QCommandLineOption(["i", "ini"])
        self.ini_option.setDescription(
            "Load settings from the provided <config.ini> file."
        )
        self.ini_option.setDefaultValue("config.ini")
        self.addOption(self.ini_option)
