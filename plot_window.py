# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Implements a window for plots."""

from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
)
from matplotlib.backends.backend_qt5agg import (
    NavigationToolbar2QT as NavigationToolbar,
)
from matplotlib.figure import Figure
from PySide2.QtCore import Qt
from PySide2.QtGui import QKeySequence
from PySide2.QtWidgets import QShortcut, QWidget

from ui_loader import UiLoader


class PlotWindow(QWidget):
    """Plots output and/or input in window."""

    def __init__(self, output=None):
        """Set up the plot window."""
        super().__init__()

        QShortcut(QKeySequence.Close, self, self.close, Qt.ApplicationShortcut)

        self.gui = UiLoader().load(":/ui/plot_window.ui")

        time_series_canvas = TimeSeriesPlot(output)
        self.gui.time_series_layout.addWidget(
            NavigationToolbar(time_series_canvas, self)
        )
        self.gui.time_series_layout.addWidget(time_series_canvas)

        frequency_canvas = FFTPlot(output)
        self.gui.fft_layout.addWidget(
            NavigationToolbar(frequency_canvas, self)
        )
        self.gui.fft_layout.addWidget(frequency_canvas)

        frequency_db_canvas = FFTdBPlot(output)
        self.gui.fft_db_layout.addWidget(
            NavigationToolbar(frequency_db_canvas, self)
        )
        self.gui.fft_db_layout.addWidget(frequency_db_canvas)

    def show(self):
        """Show the widget."""
        self.gui.show()


class TimeSeriesPlot(FigureCanvas):
    """Plots a time series."""

    def __init__(self, output):
        """Create a time series plot."""
        super().__init__(Figure())
        axs = self.figure.subplots(
            output.shape[1], sharex=True, sharey=True, squeeze=False,
        )
        for index, channel in enumerate(output.transpose()):
            axs[index, 0].plot(
                output.time_vector,
                channel,
                label="Output Ch{:d}".format(index + 1),
            )
            axs[index, 0].legend()
            axs[index, 0].set_ylim((-1.1, 1.1))
            axs[index, 0].set_xlabel("Time/s")
            axs[index, 0].set_ylabel("Signal/FS")
        # self.figure.tight_layout()


class FFTPlot(FigureCanvas):
    """Plots a FFT."""

    def __init__(self, output):
        """Create a FFT plot."""
        super().__init__(Figure())
        axs = self.figure.subplots(
            output.shape[1], sharex=True, sharey=True, squeeze=False
        )
        for index, channel in enumerate(output.transpose()):
            axs[index, 0].magnitude_spectrum(
                channel, Fs=output.rate, label="Output"
            )
            axs[index, 0].legend()
            axs[index, 0].set_xlabel("Frequency/Hz")
            axs[index, 0].set_ylabel("Amplitude/FS")
        # self.figure.tight_layout()


class FFTdBPlot(FigureCanvas):
    """Plots a FFT in dB."""

    def __init__(self, output):
        """Create a FFT plot in dB."""
        super().__init__(Figure())
        axs = self.figure.subplots(
            output.shape[1], sharex=True, sharey=True, squeeze=False
        )
        for index, channel in enumerate(output.transpose()):
            axs[index, 0].magnitude_spectrum(
                channel, Fs=output.rate, scale="dB", label="Output"
            )
            axs[index, 0].legend()
            axs[index, 0].set_xlabel("Frequency/Hz")
            axs[index, 0].set_ylabel("Amplitude/dBFS")
        # self.figure.tight_layout()
