# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Common base class of the console and gui implementation."""

from typing import List

from parse import Parser
from settings import Settings


# pylint: disable=too-few-public-methods
class Base:
    """Common base class for console and gui implementation."""

    def __init__(self, argv: List[str]):
        """Set up parser and settings.

        Parameters
        ----------
        argv : List[str]
            The command line arguments destination use.
        """
        super().__init__(argv)

        self.parser = Parser()
        self.parser.process(self)

        if self.parser.isSet(self.parser.ini_option):
            self.settings = Settings(
                None, path=self.parser.value(self.parser.ini_option),
            )
        else:
            self.settings = Settings(None)
