# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Defines the main window of the application."""

import math

import numpy as np
from PySide2.QtCore import QBuffer, QPoint, QSize, Qt, QTime
from PySide2.QtGui import QKeySequence
from PySide2.QtWidgets import QMainWindow, QShortcut

import rsc  # noqa  # pylint: disable=unused-import
from audio_device import AudioInput, AudioOutput, InputFormat
from audio_signal import AudioSignal
from plot_window import PlotWindow
from settings import Settings
from ui_loader import UiLoader
from wav import WAV, WAVDir


class MainWindow(QMainWindow):
    """Defines the main window of the application."""

    def __init__(self, parent=None):
        """Set up the QMainWindow."""
        super().__init__(parent=parent)

        self.setObjectName("main_window")

        QShortcut(QKeySequence.Quit, self, self.close, Qt.ApplicationShortcut)

        self.gui = None
        self.wav = None
        self.recorder = None
        self.default_output = None
        self.recorded_signal = None
        self.rec_window = None
        self.record = None
        self.last_volumes = None
        self.first_scale = None
        self.monitor = None
        self.plot_window = None

        # Load the UI from main_window.ui
        self.gui = UiLoader().load(":/ui/main_window.ui")
        self.setCentralWidget(self.gui)

        for device in AudioOutput.devices():
            self.gui.output_device.addItem(device.deviceName(), device)
        self.gui.output_device.setCurrentText(
            AudioOutput.default().deviceName()
        )
        self.output = AudioOutput(self.gui.output_device.currentData())
        self.gui.output_device.currentIndexChanged.connect(self.output_changed)

        for device in AudioInput.devices():
            self.gui.input_device.addItem(device.deviceName(), device)
        self.gui.input_device.setCurrentText(AudioInput.default().deviceName())
        self.input = AudioInput(self.gui.input_device.currentData())
        self.gui.input_device.currentIndexChanged.connect(self.input_changed)

        self.fill_wav_file()

        self.gui.generate_button.clicked.connect(self.generate)

        self.gui.play_button.start.connect(self.play)
        self.gui.play_button.stop.connect(self.stop)

        self.gui.type.addItems(
            [
                "White",
                "Pink",
                "Blue",
                "Brown",
                "Violet",
                "Sine",
                "Chirp",
                "Square",
                "Comb",
            ]
        )

        self.gui.plot_button.clicked.connect(self.plot)

        self.gui.start_output_test.start.connect(self.test_output)
        self.gui.start_output_test.stop.connect(self.stop_output)

        self.gui.record_button.start.connect(self.start_record)
        self.gui.record_button.start2.connect(self.play_record)
        self.gui.record_button.stop.connect(self.stop_record)

        self.gui.plot_record.clicked.connect(self.plot_record)

        self.gui.volume.valueChanged.connect(self.handle_volume_changed)
        self.gui.volume_text.valueChanged.connect(
            self.handle_volume_text_changed
        )

        self.gui.adjust_volume.start.connect(self.adjust_start)
        self.gui.adjust_volume.stop.connect(self.adjust_stop)

    def showEvent(self, event):
        """Restore state from settings."""
        with Settings(self.objectName()) as settings:
            self.resize(settings.value("size", QSize(560, 390)))
            self.move(settings.value("position", QPoint(0, 0)))
        event.accept()

    def closeEvent(self, event):
        """Store state in settings."""
        with Settings(self.objectName()) as settings:
            settings.setValue("size", self.size())
            settings.setValue("position", self.pos())
        event.accept()

    def fill_wav_file(self, file_url=None):
        """Fill the wav combo."""
        self.gui.wav_file.clear()
        files = WAVDir().files
        for file in files:
            self.gui.wav_file.addItem(file.fileName(), file)

        if file_url:
            self.gui.wav_file.setCurrentText(file_url.fileName())

    def generate(self):
        """Generate noise samples."""
        duration = self.gui.duration_edit.text()
        duration = QTime(0, 0, 0).secsTo(
            QTime.fromString(duration, "mm'm':ss's'")
        )

        signal_type = self.gui.type.currentText().lower()

        signal = AudioSignal.select(signal_type=signal_type, duration=duration)
        file = WAV(signal)
        self.fill_wav_file(file)

    def output_changed(self):
        """Update output."""
        self.output = AudioOutput(self.gui.output_device.currentData())

    def input_changed(self):
        """Update input."""
        self.input = AudioInput(self.gui.input_device.currentData())

    def play(self):
        """Play selected WAV."""
        self.wav = self.gui.wav_file.currentData()
        self.monitor = AudioOutput()
        self.monitor.idle.connect(self.stopped)
        self.monitor.start(self.wav)

    def stop(self):
        """Stop playing WAV."""
        self.monitor.stop()

    def stopped(self):
        """Playing WAV has been stopped."""
        self.gui.play_button.show_stop()

    def test_output(self):
        """Test selected output device."""
        self.wav = self.gui.wav_file.currentData()
        self.output.idle.connect(self.stopped_output)
        self.output.start(self.wav)

    def stop_output(self):
        """Stop testing/playing on selected output device."""
        self.output.stop()

    def stopped_output(self):
        """Testing/Playing of selected output device has been stopped."""
        self.gui.start_output_test.show_stop()

    def plot(self):
        """Plot the generated WAV data."""
        self.wav = self.gui.wav_file.currentData()
        self.plot_window = PlotWindow(self.wav.audio_signal)
        self.plot_window.show()

    def start_record(self):
        """Start recording on the selected input device."""
        self.recorder = AudioInput(
            self.gui.input_device.currentData(), InputFormat()
        )
        self.record = QBuffer()
        self.record.open(QBuffer.ReadWrite)
        self.wav = self.gui.wav_file.currentData()
        self.output.idle.connect(self.play_record)
        self.output.start(self.wav)
        self.recorder.start(self.record)

    @staticmethod
    def separate(data):
        """Unzips the recorded channels."""
        floats = np.frombuffer(data, dtype="float32")
        channels = InputFormat().channelCount()

        array = np.empty((len(floats) // channels, channels), dtype="float32")
        for channel in range(channels):
            array[:, channel] = floats[channel::channels]
        return array

    def play_record(self):
        """Play the recorded data."""
        self.output.idle.disconnect(self.play_record)
        self.default_output = AudioOutput()
        self.default_output.idle.connect(self.stop_record)
        self.recorder.stop()
        self.recorded_signal = AudioSignal.from_buffer(self.record)
        clipped, total = self.recorded_signal.clipped(threshold=1)
        if clipped:
            self.gui.clipped.setStyleSheet("QLabel {color : red; }")
        else:
            self.gui.clipped.setStyleSheet(
                "QLabel {color : greefrom_arrayn; }"
            )
        self.gui.clipped.setText(str(clipped) + "/" + str(total))
        self.record.seek(0)
        self.gui.record_button.show_start2()
        self.default_output.start(self.record)

    def stop_record(self):
        """Stop recording."""
        self.default_output.stop()
        self.default_output.idle.disconnect(self.stop_record)
        self.gui.record_button.show_stop()

    def plot_record(self):
        """Plot the recorded data."""
        # out_samples = self.wav.audio_signal
        in_samples = self.recorded_signal.rescaled()

        self.rec_window = PlotWindow(in_samples)
        self.rec_window.show()

    def handle_volume_changed(self):
        """Volume has changed."""
        self.output.setVolume(self.gui.volume.value())
        self.gui.volume_text.setValue(
            math.ceil(self.gui.volume.value() * 1000) / 1000.0
        )

    def handle_volume_text_changed(self):
        """Volume has changed."""
        self.gui.volume.setValue(self.gui.volume_text.value())

    def adjust_start(self):
        """Start adjusting volume."""
        self.last_volumes = [None] * 5
        self.first_scale = True
        self.adjust()

    def adjust(self):
        """Adjust volume."""
        self.recorder = AudioInput(
            self.gui.input_device.currentData(), InputFormat()
        )
        self.record = QBuffer()
        self.record.open(QBuffer.ReadWrite)
        self.wav = self.gui.wav_file.currentData()
        self.output.idle.connect(self.adjust_stop)
        self.output.start(self.wav)
        self.recorder.start(self.record)

    def adjust_stop(self):
        """Stop adjusting volume."""
        self.output.idle.disconnect(self.adjust_stop)
        self.gui.adjust_volume.show_stop()
        self.recorder.stop()
        self.output.stop()
        self.record.seek(0)
        array = self.separate(self.record.data())
        self.recorded_signal = AudioSignal(array)
        clipped, total = self.recorded_signal.clipped(threshold=0.2)

        if clipped:
            self.gui.clipped.setStyleSheet("QLabel {color : red; }")
        else:
            self.gui.clipped.setStyleSheet("QLabel {color : green; }")
        self.gui.clipped.setText(str(clipped) + "/" + str(total))

        samples = self.recorded_signal
        signal_max = np.amax(np.abs(samples))
        volume = self.gui.volume.value()

        if clipped:
            self.gui.volume.setValue(3 * volume / 4)

        elif signal_max < 0.18 and not self.first_scale:
            self.gui.volume.setValue(1.1 * volume)

        elif self.first_scale:
            self.first_scale = False
            self.gui.volume.setValue(0.185 * volume / signal_max)

        self.last_volumes.pop(0)
        self.last_volumes.append(np.min([1.0, self.gui.volume.value()]))
        print(self.last_volumes)
        volume_set = set(self.last_volumes)
        if len(volume_set) != 1:
            self.adjust()
