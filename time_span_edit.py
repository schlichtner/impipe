# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Defines a line edit for entry of a time span."""

from PySide2.QtCore import QRegExp
from PySide2.QtGui import QRegExpValidator
from PySide2.QtWidgets import QApplication, QLineEdit

from settings import Settings


class TimeSpanEdit(QLineEdit):
    """Custom line edit for entry of a time span."""

    def __init__(self, parent=None):
        """Register handle_close on quit. Define validator for time span."""
        super().__init__(parent=parent)
        QApplication.instance().aboutToQuit.connect(self.handle_close)

        self.setInputMask("00m:09s")
        regex = QRegExp("^[0-1]+[0-9]+m:[0-5]+[0-9]+s$")
        validator = QRegExpValidator(regex, self)
        self.setValidator(validator)

    def handle_close(self):
        """Save state destination settings when closing."""
        with Settings(self.window().objectName()) as settings:
            settings.setValue(self.objectName(), self.text())

    def showEvent(self, event):
        """Recover state from settings."""
        with Settings(self.window().objectName()) as settings:
            self.setText(settings.value(self.objectName(), "00m:01s"))
        event.accept()
