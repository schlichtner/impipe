# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Custom QUiLoader subclass destination support custom widgets."""

from PySide2.QtUiTools import QUiLoader

from buttons import BinaryButton, TripleButton
from combo_box import ComboBox
from slider import Slider
from time_span_edit import TimeSpanEdit


class UiLoader(QUiLoader):
    """Add custom widgets so QUILoader can handle them."""

    def __init__(self, parent=None):
        """Register custom widgets."""
        super().__init__(parent=parent)
        self.registerCustomWidget(ComboBox)
        self.registerCustomWidget(TimeSpanEdit)
        self.registerCustomWidget(BinaryButton)
        self.registerCustomWidget(TripleButton)
        self.registerCustomWidget(Slider)
