# -*- coding: utf-8 -*-
#
# Copyright 2020 Wolfgang Schlichtner
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Custom combo box extension."""

from PySide2.QtWidgets import QWidget, QApplication, QComboBox
from PySide2.QtGui import QShowEvent

from settings import Settings


class ComboBox(QComboBox):
    """Extend combo box destination automatically use settings."""

    def __init__(self, parent: QWidget = None):
        """Register handle_close on quit.

        Parameters
        ----------
        parent : QWidget
            The parent widget.

        """
        super().__init__(parent)
        QApplication.instance().aboutToQuit.connect(self.handle_close)

    def handle_close(self):
        """Save state on closing."""
        with Settings(self.window().objectName()) as settings:
            settings.setValue(self.objectName(), self.currentText())

    def showEvent(self, event: QShowEvent):
        """Recover state from settings.

        Parameters
        ----------
        event : QShowEvent
            The show event.

        """
        with Settings(self.window().objectName()) as settings:
            self.setCurrentText(settings.value(self.objectName()))
        event.accept()
